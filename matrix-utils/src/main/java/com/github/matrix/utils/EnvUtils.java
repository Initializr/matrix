package com.github.matrix.utils;

import cn.hutool.extra.spring.SpringUtil;
import org.apache.commons.lang3.StringUtils;


public final class EnvUtils {

    public static boolean isProd() {
        return StringUtils.equalsIgnoreCase(SpringUtil.getActiveProfile(), "prod") ||  StringUtils.equalsIgnoreCase(SpringUtil.getActiveProfile(), "pro");
    }
    public static boolean isNotProd() {
        return !isProd();
    }

    public static String activeProfile() {
        return SpringUtil.getActiveProfile();
    }

}
