package com.github.matrix.common.resp;

/**
 * @author zhaoguoxin
 */
public enum BaseResultCodeEnum implements BaseCodeEnum {
    SUCCESS(0, "SUCCESS"),
    FAILED(500, "系统异常");

    private final Integer code;
    private final String message;

    BaseResultCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
