package com.github.matrix.common.exception;

public class BizException extends RuntimeException {

    private static final String DEFAULT_MSG = "系统异常，请稍后重试";

    public BizException() {
        super(DEFAULT_MSG);
    }

    public BizException(String message) {
        super(message);
    }

    public BizException(Throwable cause) {
        super(cause);
    }

    public BizException(String message, Throwable cause) {
        super(message, cause);
    }

    public BizException(String message, Throwable cause,
                          boolean enableSuppression,
                          boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }


}
