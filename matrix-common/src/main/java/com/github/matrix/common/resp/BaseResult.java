package com.github.matrix.common.resp;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhaoguoxin
 */
@Data
public class BaseResult<T> implements Serializable {

    private Integer code;
    private String message;
    private String traceId;
    private String extraMessage;
    private T data;

    public BaseResult() {
    }

    public BaseResult(Integer code) {
        this.code = code;
    }

    public BaseResult(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public BaseResult(Integer code, T data) {
        this.code = code;
        this.data = data;
    }

    public BaseResult(Integer code, String message, String traceId, String extraMessage, T data) {
        this.code = code;
        this.message = message;
        this.traceId = traceId;
        this.extraMessage = extraMessage;
        this.data = data;
    }

    public static BaseResult success() {
        return new BaseResult(BaseResultCodeEnum.SUCCESS.getCode());
    }
    public static <T> BaseResult success(T data) {
        return new BaseResult(BaseResultCodeEnum.SUCCESS.getCode(), data);
    }
    public static BaseResult success(BaseCodeEnum codeEnum) {
        return new BaseResult(codeEnum.getCode());
    }

    public static BaseResult fail(BaseCodeEnum codeEnum) {
        return new BaseResult(codeEnum.getCode(), codeEnum.getMessage());
    }


}
