package com.github.matrix.common.resp;

/**
 * @author zhaoguoxin
 */
public interface BaseCodeEnum {
    /**
     * 枚举code
     * @return
     */
    Integer getCode();

    /**
     * 枚举message
     * @return
     */
    String getMessage();
}
