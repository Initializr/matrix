package com.github.winyiwin.matrix.apollo.boot.autoconfigure;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@EnableApolloConfig
public class MatrixApolloAutoConfiguration {
}
