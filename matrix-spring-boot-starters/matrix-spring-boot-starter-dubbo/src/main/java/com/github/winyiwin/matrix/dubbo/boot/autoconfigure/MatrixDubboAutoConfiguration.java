package com.github.winyiwin.matrix.dubbo.boot.autoconfigure;

import com.github.winyiwin.matrix.dubbo.swagger2.annotions.EnableSwagger2Dubbo;
import io.swagger.annotations.Api;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableDubbo
@Configuration
@EnableSwagger2Dubbo
@EnableSwagger2
public class MatrixDubboAutoConfiguration {

    @Value("${spring.application.name:app}")
    private String appName;
    @Value("${spring.application.version:1.0.1}")
    private String appVersion;

    @Value("${swagger.dubbo.enabled:true}")
    private boolean enabled;

    @Bean
    public Docket createDubboApi() {
        return new Docket(DocumentationType.SWAGGER_2).enable(enabled)
                .apiInfo(apiInfo())
                .groupName("dubbo")
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(appName + " Dubbo APIs")
                .version(appVersion)
                .description("dubbo")
                .build();
    }

}
