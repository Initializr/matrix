package com.github.winyiwin.matrix.web.boot.autoconfigure;

import com.github.matrix.utils.UtilAutoConfiguration;
import com.github.winyiwin.matrix.log.listener.MatrixLogListenerConfiguration;
import com.github.winyiwin.matrix.web.config.WebConfig;
import com.github.winyiwin.matrix.web.exception.handler.DefaultExceptionHandler;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import({WebConfig.class, DefaultExceptionHandler.class})
@AutoConfigureAfter({UtilAutoConfiguration.class, MatrixLogListenerConfiguration.class})
public class MatrixSpringBootWebAutoConfiguration {

    @Value("${spring.application.name:app}")
    private String appName;
    @Value("${spring.application.version:v1}")
    private String appVersion;
    @Value("${swagger.web.enabled:true}")
    private Boolean enabled;

    @Bean
    public Docket createWebApi() {
        return new Docket(DocumentationType.SWAGGER_2).enable(enabled)
                .apiInfo(apiInfo())
                .groupName("web")
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                .paths(PathSelectors.any())

                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(appName + " RESTful APIs")
                .version(appVersion)
                .description("更多内容请关注：YApi")
                .build();
    }
}
