package com.github.winyiwin.matrix.dubbo.swagger2.annotions;

import org.springframework.context.annotation.ComponentScan;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * @author zhaoguoxin
 */
@Retention(value = java.lang.annotation.RetentionPolicy.RUNTIME)
@Target(value = {java.lang.annotation.ElementType.TYPE})
@Documented
@ComponentScan(basePackages = {"com.github.winyiwin.matrix.dubbo.swagger2.config", "com.github.winyiwin.matrix.dubbo.swagger2.controller" ,"com.github.winyiwin.matrix.dubbo.swagger2.provider"})
public @interface EnableSwagger2Dubbo {
}
