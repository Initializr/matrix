# 简介
matrix是一个快速构建springboot 项目的脚手架
## 优点
- 父项目内置了三个maven profile (dev/test/prod),并设置打包名称生成规则方式，方便环境区分,默认激活dev,并提供根据profiledeploy不同版本号的jar包和依赖不同版本的jar包的功能
  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0121/172550_8875793a_484241.png "屏幕截图.png")

- 项目对外jar包 

  | 环境 | 版本号        |      |
  | ---- | ------------- | ---- |
  | dev  | -DEV-SNAPSHOT |      |
  | test | -SNAPSHOT     |      |
  | prod | .RELEASE      |      |

  父项目版本定义
  
  ```xml
  <properties>
       <revision>1.0.0${ver_type}</revision>
  </properties>
  ```
  
  build节点添加
  
  ```xml
  <plugin>
      <groupId>org.codehaus.mojo</groupId>
      <artifactId>flatten-maven-plugin</artifactId>
      <version>${flatten-maven-plugin.version}</version>
      <inherited>true</inherited>
      <executions>
          <execution>
              <id>flatten</id>
              <phase>process-resources</phase>
              <goals>
                  <goal>flatten</goal>
              </goals>
              <configuration>
                  <updatePomFile>true</updatePomFile>
                  <flattenMode>oss</flattenMode>
                  <pomElements>
                      <name/>
                      <description>remove</description>
                      <developers>remove</developers>
                      <scm>remove</scm>
                      <url>remove</url>
                  </pomElements>
              </configuration>
          </execution>
          <execution>
              <id>flatten-clean</id>
              <phase>clean</phase>
              <goals>
                  <goal>clean</goal>
              </goals>
          </execution>
      </executions>
  </plugin>
  ```
  
  
## 项目结构
- matrix-dependencies-bom matrix项目依赖管理
- matrix-spring-boot-starters  starters 集合
- matrix-swagger2  swagger2 依赖pom
- matrix-parent starter 父项目
- matrix-common 公共封装
- matrix-utils 工具类集合
## 发布中央仓库
```bash
mvn clean deploy -P release
```
## 使用

1. 项目中引入parent

```xml
<parent>
    <groupId>com.github.winyiwin</groupId>
    <artifactId>matrix-spring-boot-starter-parent</artifactId>
    <version>${latest.version}</version> 
</parent>
```
> 引入parent之后

2. 若子项目需要上传nexus时需要一并添加源码则需要在该子项目下添加单独配置

```xml
<build>
    <plugins>
        <plugin>
            <artifactId>maven-source-plugin</artifactId>
        </plugin>
    </plugins>
</build>
```

3. application.yml或bootstrap.yml等配置文件配置激活spring.profiles.active 

   > 可使用框架自带的maven配置profileActive激活，也可自定义环境

```yaml
spring:
  profiles:
    active: @profileActive@
```
4. main jar 设置
```xml
<build>
     <finalName>${project.name}</finalName>
     <plugins>
         <plugin>
             <groupId>org.springframework.boot</groupId>
             <artifactId>spring-boot-maven-plugin</artifactId>
         </plugin>
     </plugins>
</build>
```
5. swagger 

```xml
<dependency>
            <groupId>com.github.winyiwin</groupId>
            <artifactId>matrix-spring-boot-starter-dubbo</artifactId>
        </dependency>
        <dependency>
            <groupId>com.github.winyiwin</groupId>
            <artifactId>matrix-spring-boot-starter-web</artifactId>
        </dependency>
```
两个依赖分别提供dubbo 和 web的swagger , dubbo 目前使用的nacos,请自行更换
http://localhost:9999/doc.html
![knif4j](https://images.gitee.com/uploads/images/2021/0121/172134_bd4588ac_484241.png "屏幕截图.png")
http://localhost:9999/swagger-ui.html
![swagger](https://images.gitee.com/uploads/images/2021/0121/172230_f52b5fab_484241.png "屏幕截图.png")


